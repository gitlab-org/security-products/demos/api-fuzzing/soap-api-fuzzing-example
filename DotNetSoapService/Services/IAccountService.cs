﻿using System.ServiceModel;

namespace DotNetSoapService.Services
{
    [ServiceContract]
    public interface IAccountService
    {
        [OperationContract]
        Account CreateAccount(Account account);

        [OperationContract]
        Account GetAccountById(int userId);

        [OperationContract]
        Account GetAccountByUser(string user);

        [OperationContract]
        void UpdateAccount(Account account);

        [OperationContract]
        Account[] GetAllAccounts();

        [OperationContract]
        void DeleteAccount(int userId);

        [OperationContract]
        void DeleteAllAccounts();
    }
}
